Trello TaskImporter
===================

1. Go to `chrome://extensions <chrome://extensions>`_.
2. Click "Developer mode" in the top right hand corner.
3. Click "Load Unpacked Extension".
4. Choose the folder that contains repo files.
5. Extension should loaded into your browser.