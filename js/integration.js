function authenticateAccount(isInteractive, authenticationSuccess, authenticationFailure) {
    Trello.setKey(getApiKey());
    Trello.authorize({
        type: 'redirect',
        name: 'Trello TaskImporter',
        scope: {read: 'true', write: 'true'},
        expiration: 'never',
        interactive: isInteractive,
        success: authenticationSuccess,
        error: authenticationFailure
    });
}

function getBoards(success, error) {
    Trello.get('/member/me/boards', success, error);
}

function getLists(boardId, success, error) {
    Trello.get('/boards/' + boardId + '/lists', success, error);
}

function addCard(listId, name, url, success, error) {
    var newCard = {
        name: name,
        pos: 'top',
        due: null,
        idList: listId,
        urlSource: url
    };
    Trello.post('/cards/', newCard, success, error);
}

function getApiCredentials(success, error) {
    $.ajax({ url: 'https://trello.com/1/appKey/generate' }).done(function (data) {
        var html = $($.parseHTML(data)),
            apiKey = html.find('input#key').val();
        if (!apiKey) {
            error('Couldn\'t get API key from the Trello.');
        } else {
            success(apiKey);
        }
    }).fail(function () {
        error('First, you must log in to Trello and then try to connect again.');
    });
}